import math as m
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator

#Initialise panel parmaeters
panel_width = [0.567, 1.174, 1.741]
albedo = 0.3
distance_n = 20
height_n = 20
dist_in_panels = 10
max_height = 5

#Sun angles
sun_deg = [
89,
77,
66,
55,
43,
30,
17,
3,
11,
25,
38,
51,
62,
73,
84,
80,
69,
57,
45,
32,
19,
4,
10,
25,
38,
50,
62,
73,
85,
83,
72,
60,
48,
35,
21,
5,
10,
25,
39,
52,
64,
75,
87,
86,
75,
63,
50,
37,
22,
6,
10,
26,
40,
54,
66,
78,
89,
89,
77,
65,
52,
38,
23,
6,
11,
28,
43,
57,
69,
81,
80,
68,
54,
40,
23,
5,
13,
31,
46,
60,
73,
85,
83,
71,
57,
41,
24,
4,
16,
35,
51,
65,
78,
85,
73,
59,
43,
24,
3,
19,
38,
55,
70,
82,
88,
76,
62,
45,
25,
2,
21,
42,
59,
74,
86,
79,
65,
48,
27,
2,
23,
45,
62,
77,
89,
81,
68,
51,
29,
3,
24,
47,
65,
79,
83,
69,
52,
31,
4,
24,
47,
65,
79,
83,
70,
53,
32,
6,
22,
46,
64,
78,
83,
69,
53,
32,
6,
20,
44,
62,
77,
89,
80,
66,
50,
30,
6,
19,
41,
59,
74,
86,
89,
77,
63,
47,
27,
5,
18,
39,
57,
71,
84,
85,
72,
58,
42,
24,
3,
18,
37,
54,
69,
81,
80,
68,
54,
38,
20,
1,
18,
36,
52,
66,
79,
87,
76,
63,
49,
34,
17,
1,
19,
35,
50,
64,
76,
88,
84,
72,
59,
46,
31,
15,
2,
18,
34,
49,
62,
74,
86,
79,
68,
56,
43,
29,
13,
2,
18,
33,
46,
59,
71,
83,
88,
77,
66,
54,
41,
28,
13,
2,
17,
31,
44,
57,
68,
80,
87,
76,
64,
53,
41,
28,
14,
1,
15,
29,
42,
54,
66,
77,
88,
87,
76,
65,
53,
41,
28,
15,
1,
13,
27,
40,
52,
64,
75,
86
]

#Create an array of linearly spaced panel height values between two values
height = np.linspace(0, max_height, height_n)

#Initialise panels angle arrays
best_bifacial_sun = np.zeros((len(panel_width), height_n, distance_n, len(sun_deg)))
best_monofacial_sun = np.zeros((len(panel_width), height_n,  distance_n, len(sun_deg)))
best_bifacial_angles = np.zeros((len(panel_width), height_n,  distance_n, len(sun_deg)))
best_monofacial_angles = np.zeros((len(panel_width), height_n, distance_n, len(sun_deg)))
best_bifacial_distance_sum = 0
best_monofacial_distance_sum = 0

#Set sun angle in radians
sun_angle = [0]*len(sun_deg)
for i in range(len(sun_deg)):
    sun_angle[i] = m.pi*sun_deg[i]/180


#Iterate to find optimal solar panel angles, seperation and height for each panel width over all sun angles in period
for w in range(len(panel_width)):
    #Create an array of linearly spaced panel seperation values between two values
    distance = np.linspace(panel_width[w], dist_in_panels*panel_width[w], distance_n)

    #Iterate to find optimal solar panel angles, seperation for each panel height and width over all sun angles in period
    for h in range(len(height)):
        
        #Ensure panel cannot tilt into ground
        if panel_width[w] > height[h]: 
            max_angle = -m.ceil(180*m.asin(height[h]/panel_width[w])/m.pi)

        else:
            max_angle = -90

        #Iterate to find optimal solar panel angles for each panel height, width and  seperation over all sun angles in period
        for j in range(len(distance)):

            #Find optimum solar panel angle for the given panel dimensions and sun angle
            for time in range(len(sun_deg)):
                
                #Iterates through ever solar panel angle to fine optimum for the given solar angle
                for panel_deg in range(0, max_angle, -1):
                    
                    #Conver panel angle to radians
                    panel_angle = m.pi*panel_deg/180

                    #Limit coordinates for direct light
                    class direct_limits :
                        x = [ 
                            #x Limits from edge of solar panel
                            ((m.cos(sun_angle[time])**2) * (-distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) - height[h] * m.sin(sun_angle[time])) / ((-m.cos(sun_angle[time])**2) - (m.sin(sun_angle[time])**2)),
                            (-(m.cos(sun_angle[time])**2) * (distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) - height[h] * m.sin(sun_angle[time])) / ((-m.cos(sun_angle[time])**2) - (m.sin(sun_angle[time])**2)),
                            #x Limit from shade of neighbouring panel
                            ((m.cos(sun_angle[time])**2) * (-3*distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) - height[h] * m.sin(sun_angle[time])) / ((-m.cos(sun_angle[time])**2) - (m.sin(sun_angle[time])**2))
                        ]

                        y = [
                            #y Limits from edge of solar panel
                            (m.cos(sun_angle[time])*m.sin(sun_angle[time]) * (-distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.sin(sun_angle[time]) + height[h] * m.cos(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2)),
                            (-m.cos(sun_angle[time])*m.sin(sun_angle[time]) * (distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.sin(sun_angle[time]) + height[h] * m.cos(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2)),
                            #y Limit from shade of neighbouring panel
                            (m.cos(sun_angle[time])*m.sin(sun_angle[time]) * (-3*distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.sin(sun_angle[time]) + height[h] * m.cos(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2))
                        ]

                    #Limit coordinates for reflected light
                    class reflected_limits :
                        x = [
                            #X Limit from the reflected light fitting between the panels
                            (m.cos(sun_angle[time])*(-3*distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time]) + 2*height[h]*m.sin(sun_angle[time]) - distance[j]*m.tan(sun_angle[time])) / (-m.cos(sun_angle[time]) - m.tan(sun_angle[time])*m.sin(sun_angle[time])),
                            (-(m.cos(sun_angle[time])**2)*(distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) + 2*height[h]*m.sin(sun_angle[time])*m.cos(sun_angle[time]) - distance[j]*m.sin(sun_angle[time])) / (-(m.cos(sun_angle[time])**2) - (m.sin(sun_angle[time])**2)),
                            #X Limit from the reflected light onto the edges of the solar panel
                            ((m.cos(sun_angle[time])**2)*(distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) + distance[j]*m.sin(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2)),
                            ((m.cos(sun_angle[time])**2)*(distance[j]-panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time])*m.cos(sun_angle[time]) + distance[j]*m.sin(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2))
                        ]

                        y = [
                            #Y Limit from the reflected light fitting between the panels
                            (m.cos(sun_angle[time])*(-3*distance[j]+panel_width[w]*m.cos(panel_angle)) - panel_width[w]*m.sin(panel_angle)*m.sin(sun_angle[time]) + 2*height[h]*m.sin(sun_angle[time]) + distance[j]/m.tan(sun_angle[time])) / (-m.sin(sun_angle[time]) - m.cos(sun_angle[time])/m.tan(sun_angle[time])),
                            (-m.cos(sun_angle[time])*m.sin(sun_angle[time])*(distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*(m.sin(sun_angle[time])**2) + 2*height[h]*(m.sin(sun_angle[time])**2) + distance[j]*m.cos(sun_angle[time])) / (-(m.cos(sun_angle[time])**2) - (m.sin(sun_angle[time])**2)),
                            #Y Limit from the reflected light onto the edges of the solar panel
                            (m.cos(sun_angle[time])*m.sin(sun_angle[time])*(distance[j]+panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*(m.sin(sun_angle[time])**2) - distance[j]*m.cos(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2)),
                            (m.cos(sun_angle[time])*m.sin(sun_angle[time])*(distance[j]-panel_width[w]*m.cos(panel_angle)) + panel_width[w]*m.sin(panel_angle)*(m.sin(sun_angle[time])**2) - distance[j]*m.cos(sun_angle[time])) / ((m.cos(sun_angle[time])**2) + (m.sin(sun_angle[time])**2))
                        ]

                    #Choose the appropriate limits
                    if reflected_limits.x[0] < reflected_limits.x[2] and reflected_limits.x[0] < reflected_limits.x[3] and reflected_limits.x[1] < reflected_limits.x[2] and reflected_limits.x[1] < reflected_limits.x[3]:
                        #No direct sunlight reflects onto panel
                        back_width_cover = 0

                    else:

                        reflected_limits.x.sort()
                        reflected_limits.y.sort()
                        #Calculate the reflected sun from distance between middle limits
                        back_width_cover = albedo * m.sqrt(((reflected_limits.x[1]-reflected_limits.x[2])**2) + ((reflected_limits.y[1]-reflected_limits.y[2])**2))

                    #Calculate the direct sun from distance between closest of the two limits to "left most" limit (see desmos model)
                    front_width_cover = min(m.sqrt(((direct_limits.x[0]-direct_limits.x[1])**2) + ((direct_limits.y[0]-direct_limits.y[1])**2)),
                                            m.sqrt(((direct_limits.x[0]-direct_limits.x[2])**2) + ((direct_limits.y[0]-direct_limits.y[2])**2)))

                    #Find sun:panel ratio for bifacial and monofacial
                    bifacial_sun_per_panel = (front_width_cover+back_width_cover) / (2*panel_width[w])
                    monofacial_sun_per_panel = front_width_cover / (2*panel_width[w])
                    
                    #Determine if current sun:panel ratio is best for the given sun angle and given parameters
                    if bifacial_sun_per_panel > best_bifacial_sun[w][h][j][time]:
                        best_bifacial_sun[w][h][j][time] = bifacial_sun_per_panel
                        best_bifacial_angles[w][h][j][time] = panel_deg
                    
                    if monofacial_sun_per_panel > best_monofacial_sun[w][h][j][time]:
                        best_monofacial_sun[w][h][j][time] = monofacial_sun_per_panel
                        best_monofacial_angles[w][h][j][time] = panel_deg

        #If the given parameters provide the best sun:panel ratio over all time choose parameters - Pointless given graphs
        if sum(best_bifacial_sun[w][h][j]) > best_bifacial_distance_sum:
            best_bifacial_distance_sum = sum(best_bifacial_sun[w][h][j])
            best_bifacial_distance = distance[j]
            best_bifacial_height = height[h]
            best_bipanel_width = panel_width[w]

        if sum(best_monofacial_sun[w][h][j]) > best_monofacial_distance_sum:
            best_monofacial_distance_sum = sum(best_monofacial_sun[w][h][j])
            best_mono_distance = distance[j]
            best_monofacial_height = height[h]
            best_monopanel_width = panel_width[w]


print("bifacial angles:", best_bifacial_angles,
      

"\n\n\n\nbifacial panel sun per panel:", best_bifacial_distance_sum, 
"\nmonofacial sun per panel:", best_monofacial_distance_sum,

"\n\nbifacial height:", best_bifacial_height,
"\nmonofacial height:", best_monofacial_height,

"\n\nbifacial panel width:", best_bipanel_width,
"\nmonofacial panel width:", best_monopanel_width,

"\n\nbifacial distance:", best_bifacial_distance,
"\nmonofacial distance:", best_mono_distance,

"\n\nSun angles:", sun_deg)

#Plot Mesh graphs
height_mesh, distance_mesh = np.meshgrid(height, distance)

fig = plt.figure()
ax1 = fig.add_subplot(2,3,1, projection='3d')
ax1.plot_surface(height_mesh, distance_mesh, sum(np.transpose(best_bifacial_sun[0]))/len(sun_deg), cmap=cm.coolwarm)
ax1.set_xlabel("Height")
ax1.set_ylabel("Distance")
ax1.set_zlabel("% Sun Power per m^2 panel")
title1 = "Panel Width = {:}".format(panel_width[0])
ax1.set_title(title1)

ax2 = fig.add_subplot(2,3,2, projection='3d')
ax2.plot_surface(height_mesh, distance_mesh, sum(np.transpose(best_bifacial_sun[1]))/len(sun_deg), cmap=cm.coolwarm)
title2 = "Panel Width = {:}".format(panel_width[1])
ax2.set_title(title2)

ax3 = fig.add_subplot(2,3,3, projection='3d')
ax3.plot_surface(height_mesh, distance_mesh, sum(np.transpose(best_bifacial_sun[2]))/len(sun_deg), cmap=cm.coolwarm)
title3 = "Panel Width = {:}".format(panel_width[2])
ax3.set_title(title3)

def on_move(event):
    if event.inaxes == ax1:
        if ax1.button_pressed in ax1._rotate_btn:
            ax2.view_init(elev=ax1.elev, azim=ax1.azim)
            ax3.view_init(elev=ax1.elev, azim=ax1.azim)
        elif ax1.button_pressed in ax1._zoom_btn:
            ax2.set_xlim3d(ax1.get_xlim3d())
            ax2.set_ylim3d(ax1.get_ylim3d())
            ax2.set_zlim3d(ax1.get_zlim3d())
            ax3.set_xlim3d(ax1.get_xlim3d())
            ax3.set_ylim3d(ax1.get_ylim3d())
            ax3.set_zlim3d(ax1.get_zlim3d())
    elif event.inaxes == ax2:
        if ax2.button_pressed in ax2._rotate_btn:
            ax1.view_init(elev=ax2.elev, azim=ax2.azim)
            ax3.view_init(elev=ax2.elev, azim=ax2.azim)
        elif ax2.button_pressed in ax2._zoom_btn:
            ax1.set_xlim3d(ax2.get_xlim3d())
            ax1.set_ylim3d(ax2.get_ylim3d())
            ax1.set_zlim3d(ax2.get_zlim3d())
            ax3.set_xlim3d(ax2.get_xlim3d())
            ax3.set_ylim3d(ax2.get_ylim3d())
            ax3.set_zlim3d(ax2.get_zlim3d())
    elif event.inaxes == ax3:
        if ax3.button_pressed in ax3._rotate_btn:
            ax1.view_init(elev=ax3.elev, azim=ax3.azim)
            ax2.view_init(elev=ax3.elev, azim=ax3.azim)
        elif ax3.button_pressed in ax3._zoom_btn:
            ax1.set_xlim3d(ax3.get_xlim3d())
            ax1.set_ylim3d(ax3.get_ylim3d())
            ax1.set_zlim3d(ax3.get_zlim3d())
            ax2.set_xlim3d(ax3.get_xlim3d())
            ax2.set_ylim3d(ax3.get_ylim3d())
            ax2.set_zlim3d(ax3.get_zlim3d())
    else:
        return
    fig.canvas.draw_idle()

c1 = fig.canvas.mpl_connect('motion_notify_event', on_move)

plt.show()
